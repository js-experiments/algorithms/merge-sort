const observable = factory => (...args) => {
  const observers = [];
  const obj = factory(...args);
  const notify = (event, ...args) =>
    observers
      .filter(o => o.event === event)
      .forEach(o => o.callback.apply(obj, [event, ...args]));

  return Object.assign(
    {
      subscribe(event, callback) {
        observers.push({
          event,
          callback,
        });
      },
      notify,
    },
    obj,
  );
};

const mergeSort = observable(() => {
  return {
    list: [],
    add(n) {
      this.list.push(n);
      this.notify('add', n);
    },
    merge(left, right) {
      let i = 0;
      let j = 0;
      const merged = [];

      while (i < left.length && j < right.length) {
        if (left[i] <= right[j]) {
          merged.push(left[i]);
          i++;
        } else {
          merged.push(right[j]);
          j++;
        }
      }

      return [...merged, ...left.slice(i), ...right.slice(j)];
    },
    sort(list = this.list) {
      if (list.length <= 1) {
        return list;
      }

      const middle = Math.floor(list.length / 2);
      const left = list.slice(0, middle);
      const right = list.slice(middle);
      this.notify('set-left', left);
      this.notify('set-right', right);

      const mergedLeft = this.sort(left);
      const mergedRight = this.sort(right);
      const merged = this.merge(mergedLeft, mergedRight);

      this.notify('set-merged-left', mergedLeft);
      this.notify('set-merged-right', mergedRight);
      this.notify('set-merged', merged);

      return merged;
    },
  };
});

const randomArr = size => {
  const list = [];
  let count = 0;
  while (count < size) {
    let rand = Math.floor(Math.random() * size + 1);
    while (list.indexOf(rand) > -1) {
      rand = Math.floor(Math.random() * size + 1);
    }
    list.push(rand);
    count++;
  }

  return list;
};

const getTranslate = (str, axis = 'x') => {
  return str === ''
    ? 0
    : parseFloat(
        str
          .split('translate3d')[1]
          .replace(/, /gi, ',')
          .split(' ')[0]
          .replace(/\(|\)/gi, '')
          .split(',')[axis === 'x' ? 0 : axis === 'y' ? 1 : 2],
      );
};

const renderer = (sorter, toAdd, mainElm = null) => {
  // DOM Elements
  const mainElement = mainElm || document.getElementById('main');
  const speedElement = document.getElementById('speed');
  const algorithmContainer = document.createElement('section');
  const algorithmElement = document.createElement('div');

  let toAnimate = [];
  let intervalId;
  let speed = +speedElement.value;
  let nextAnimation;
  let added = 0;

  speedElement.addEventListener('change', () => {
    clearInterval(intervalId);
    speed = +speedElement.max + +speedElement.min - +speedElement.value;
    render(nextAnimation);
  });

  const onAdd = (type, value) => {
    toAnimate.push({
      type,
      value: [value],
    });
  };

  const onSetLeft = (type, left) => {
    toAnimate.push({
      type,
      value: left,
    });
  };

  const onSetRight = (type, right) => {
    toAnimate.push({
      type,
      value: right,
    });
  };

  const onSetMerged = (type, merged) => {
    toAnimate.push({
      type,
      value: merged,
    });
  };

  const createValueElement = v => {
    const modifiers = [
      '--top',
      '--bottom',
      '--front',
      '--back',
      '--left',
      '--right',
    ];
    const mainElement = document.createElement('div');
    mainElement.classList.add('element', 'box', `value_${v}`);
    const faces = Array.from(Array(6), (item, index) => {
      const face = document.createElement('div');
      face.classList.add('box__face', `box__face${modifiers[index]}`);
      face.textContent = v;
      mainElement.appendChild(face);

      return face;
    });

    return mainElement;
  };

  const animateList = (list, {xIni, yIni, zIni}) => {
    clearInterval(intervalId);
    const animating = [];
    const onElementAnimationEnd = event => {
      animating.pop();
      event.target.removeEventListener('transitionend', onElementAnimationEnd);
      if (animating.length === 0) {
        render();
      }
    };

    list.forEach((element, index) => {
      const x = getTranslate(element.style.transform) || 0;
      const y = getTranslate(element.style.transform, 'y') || 0;
      const z = getTranslate(element.style.transform, 'z') || 0;

      const end = `translate3d(${xIni +
        index * 100}px, ${yIni}px, ${zIni}px) rotateX(0) rotateZ(0)`;

      setTimeout(() => {
        element.style.transition = `transform ${speed}ms ease-out`;
        element.style.transitionDelay = `${speed * index}ms`;
        element.style.transform = end;
        element.addEventListener('transitionend', onElementAnimationEnd);
        animating.push(element);
      }, speed * 0.4);
    });
  };

  const render = function(next) {
    nextAnimation = next;
    intervalId = setTimeout(() => {
      const animation = toAnimate.shift();

      if (!animation) {
        clearInterval(intervalId);
        speedElement.disabled = false;
        if (next) {
          return next.call(this);
        }

        return;
      }

      const {value, type} = animation;
      const elements = value.map(
        v => document.querySelector(`.value_${v}`) || createValueElement(v),
      );

      switch (type) {
        case 'add':
          elements.forEach(e => {
            algorithmElement.appendChild(e);
            e.style.transform = `translate3d(${added *
              100}px, 0, 0) rotateX(0) rotateZ(0)`;
          });
          animateList(elements, {xIni: added * 100, yIni: 0, zIni: -1000});
          added++;
          break;
        default:
          const firstX = getTranslate(elements[0].style.transform);
          const lastX = getTranslate(
            elements[elements.length - 1].style.transform,
          );
          let xIni = 0;
          let yIni = getTranslate(elements[0].style.transform, 'y') + 100;
          const zIni = getTranslate(elements[0].style.transform, 'z') + 100;
          // [2,5,8,9,7,4,10,6,1,3]

          if (type === 'set-left' || type === 'set-merged-left') {
            xIni = firstX - 50 <= -50 ? -50 : firstX - 50;
          } else if (type === 'set-right' || type === 'set-merged-right') {
            xIni = lastX + 50 >= window.innerWidth - 50 ? firstX : firstX + 50;
          } else {
            xIni = toAnimate.length === 0 ? 0 : firstX;
            yIni =
              elements.reduce((posY, element) => {
                return Math.max(
                  posY,
                  getTranslate(element.style.transform, 'y'),
                );
              }, 100) + 100;
          }

          animateList(elements, {xIni, yIni, zIni});
      }
    }, speed);
  };

  sorter.subscribe('add', (e, value) => onAdd(e, value));
  sorter.subscribe('set-left', (e, left) => onSetLeft(e, left));
  sorter.subscribe('set-right', (e, right) => onSetRight(e, right));

  sorter.subscribe('set-merged', (e, merged) => onSetMerged(e, merged));

  return {
    renderAddSort() {
      toAdd.forEach(n => sorter.add(n));

      sorter.sort();

      return this;
    },

    start(next) {
      algorithmContainer.classList.add('algorithm');
      algorithmElement.classList.add('algorithm__elements');
      algorithmContainer.appendChild(algorithmElement);
      mainElement.appendChild(algorithmContainer);
      render.apply(this, [next]);

      // Clean list in an ugly way ;)
      sorter.list = [];
      speedElement.disabled = true;

      return this;
    },
    stop() {
      toAnimate = [];
      added = 0;
      clearInterval(intervalId);
      algorithmElement.innerHTML = '';
      algorithmElement.remove();
      algorithmContainer.innerHTML = '';
      algorithmContainer.remove();

      return this;
    },
  };
};

const arr = randomArr(10);

const algorithmRenderer = renderer(mergeSort(), arr);

const menu = document.getElementById('algorithm-menu');
const menuItems = menu.querySelectorAll('li');
const onClickMenuItem = function(e) {
  const action = this.dataset.renderer;

  algorithmRenderer
    .stop()
    .start()
    [action]();
};

menuItems.forEach(item => item.addEventListener('click', onClickMenuItem));
